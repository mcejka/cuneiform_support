# UGS

RCNN

Name of the models:

[1][2][3][4][5][6][7][8]

[1]... C = trained on cuneiforms, A = trained on airplanes
[2]... 2 = image size 224, 4 = image size 416
[3]... V = vgg architecture, B = build from scratch
[4]... I = imagenet, W = None initial weights
[5]... 0 = no transfer learning, 1 = layer ignored, 2 = two layers ignored, ...
[6]... V = validation set existed, T = validation set made by test_split
[7]... A = Adam optimizer, R = RMSprop optimizer
[8]... E = Check & Early stopping, F = Full learning
[9]... [epochs-steps-max_samples]
